# Error Code information

We have the following error code being used in the application.

```
BAD_REQUEST: {
  error: 'E3000',
  message: 'Request can\'t be processed.'
},
UNPROCESSABLE_ENTITY: {
  error: 'E3001',
  message: 'Invalid data structure.'
},
INTERNAL_SERVER_ERROR: {
  error: 'E3003',
  message: 'Some internal error occured. Please contact site administrator.',
},
NOT_AUTHORISED: {
  error: 'E3004',
  message: 'Invalid credentials'
},
FORBIDDEN: {
  error: 'E3005',
  message: 'You are not authorized to access this URL'
},
DUPLICATE_ENTRY: {
  error: 'E3006',
  message: 'Data entry already exists.'
},
NOT_FOUND: {
  error: '400',
  message: '404 ! Looks like you hit a wrong URL.'
}
```