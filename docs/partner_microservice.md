# Partner API

Domain: activedev.westus.cloudapp.azure.com

## Endpoint: /api/v1/partners/

### GET

- GET all partners (Should be called by Admin Console)

/api/v1/partners/

Response
```JSON
[
    {
        "status": "active",
        "_id": "5ab674a6a8d85845e1665213",
        "title": "Golmes Elementry",
        "email": "golmes@gmail.com",
        "phone": "(+91)789-572-7782",
        "address": {
            "streetNumber": "4038",
            "street": "Budwing Terrace",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94538",
            "full": "4038 Budwing Terrace, Fremont, CA 94538, USA",
            "lat": 37.51317,
            "lng": -121.952594,
            "latLng": "37.51317,-121.952594"
        },
        "createdAt": "2018-03-24T15:54:14.129Z",
        "updatedAt": "2018-03-24T15:54:14.129Z"
    },
    {
        "status": "inactive",
        "_id": "5ab6766da8d85845e1665214",
        "title": "California School for the Deaf",
        "email": "info@csdrcde.gov",
        "phone": "(+1)-951-248-7700",
        "address": {
            "streetNumber": "3044",
            "street": "Horace Street",
            "city": "Riverside",
            "county": "Riverside County",
            "state": "California",
            "zipCode": "92506",
            "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
            "lat": 33.9413787,
            "lng": -117.3800547,
            "latLng": "33.9413787,-117.3800547"
        },
        "createdAt": "2018-03-24T16:01:49.925Z",
        "updatedAt": "2018-03-24T16:01:49.925Z"
    }
]
```

### GET based on status

/api/v1/partners/?status=inactive

Response
```JSON
[
    {
        "status": "inactive",
        "_id": "5ab6766da8d85845e1665214",
        "title": "California School for the Deaf",
        "email": "info@csdrcde.gov",
        "phone": "(+1)-951-248-7700",
        "address": {
            "streetNumber": "3044",
            "street": "Horace Street",
            "city": "Riverside",
            "county": "Riverside County",
            "state": "California",
            "zipCode": "92506",
            "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
            "lat": 33.9413787,
            "lng": -117.3800547,
            "latLng": "33.9413787,-117.3800547"
        },
        "createdAt": "2018-03-24T16:01:49.925Z",
        "updatedAt": "2018-03-24T16:01:49.925Z"
    }
]
```

### POST

POST request on this endpoint will create a new partner in the system.

POST object body:
```JSON
{
	"title": "California School for the Deaf",
	"email": "info@csdrcde.gov",
	"phone": "(+1)-951-248-7700",
	"address": {
		"streetAddress": "3044 Horace Street",
		"city": "Riverside",
		"state": "CA",
		"zipCode": 92506
	}
}
```

Endpoint Response:
```JSON
{
    "title": "California School for the Deaf",
    "email": "info@csdrcde.gov",
    "phone": "(+1)-951-248-7700",
    "address": {
        "streetNumber": "3044",
        "street": "Horace Street",
        "city": "Riverside",
        "county": "Riverside County",
        "state": "California",
        "zipCode": "92506",
        "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
        "lat": 33.9413787,
        "lng": -117.3800547,
        "latLng": "33.9413787,-117.3800547"
    },
    "status": "active",
    "_id": "5ab6766da8d85845e1665214",
    "createdAt": "2018-03-24T16:01:49.925Z",
    "updatedAt": "2018-03-24T16:01:49.925Z"
}
```


## Endpoint: /api/v1/partners/:partnerid

This endpoint retrieve a certain partner info from the system. It can be used to update/retrieve a certain attribute as well by chnaging the Request Method type and query fields.

Supported REQUEST Methods:
- GET
- PUT
- DELETE

### GET

Example endpoints:
- /api/v1/partners/5ab6766da8d85845e1665214

Response:
```JSON
{
    "title": "California School for the Deaf",
    "email": "info@csdrcde.gov",
    "phone": "(+1)-951-248-7700",
    "address": {
        "streetNumber": "3044",
        "street": "Horace Street",
        "city": "Riverside",
        "county": "Riverside County",
        "state": "California",
        "zipCode": "92506",
        "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
        "lat": 33.9413787,
        "lng": -117.3800547,
        "latLng": "33.9413787,-117.3800547"
    },
    "status": "active",
    "_id": "5ab6766da8d85845e1665214",
    "createdAt": "2018-03-24T16:01:49.925Z",
    "updatedAt": "2018-03-24T16:01:49.925Z"
}
```

### GET a certain attribute

Example endpoints:
- /api/v1/partners/5ab6766da8d85845e1665214?fields=title,email.zipCode,address.latLng

Response:
```JSON
{
    "_id": "5ab6766da8d85845e1665214",
    "title": "California School for the Deaf",
    "email": "info@csdrcde.gov",
    "address": {
        "latLng": "33.9413787,-117.3800547"
    }
}
```

### PUT

This Method can be used to update the partner attributes.

Example endpoint:
- /api/v1/partners/5ab6766da8d85845e1665214

For updating the status the request object body:
```JSON
{
  "status": "inactive"
}
```

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab6766da8d85845e1665214",
    "title": "California School for the Deaf",
    "email": "info@csdrcde.gov",
    "phone": "(+1)-951-248-7700",
    "address": {
        "streetNumber": "3044",
        "street": "Horace Street",
        "city": "Riverside",
        "county": "Riverside County",
        "state": "California",
        "zipCode": "92506",
        "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
        "lat": 33.9413787,
        "lng": -117.3800547,
        "latLng": "33.9413787,-117.3800547"
    },
    "createdAt": "2018-03-24T16:01:49.925Z",
    "updatedAt": "2018-03-24T16:01:49.925Z"
}
```

### DELETE

This method simply updates the status of the Partner to inactive.

Example endpoint:
- /api/v1/partners/5ab6766da8d85845e1665214

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab6766da8d85845e1665214",
    "title": "California School for the Deaf",
    "email": "info@csdrcde.gov",
    "phone": "(+1)-951-248-7700",
    "address": {
        "streetNumber": "3044",
        "street": "Horace Street",
        "city": "Riverside",
        "county": "Riverside County",
        "state": "California",
        "zipCode": "92506",
        "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
        "lat": 33.9413787,
        "lng": -117.3800547,
        "latLng": "33.9413787,-117.3800547"
    },
    "createdAt": "2018-03-24T16:01:49.925Z",
    "updatedAt": "2018-03-24T16:01:49.925Z"
}
```