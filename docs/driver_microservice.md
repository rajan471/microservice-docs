# Driver API

Domain: activedev.westus.cloudapp.azure.com

## Endpoint: /api/v1/drivers/

### GET

- GET all drivers (Should be called by Admin Console)

/api/v1/drivers/

Response
```JSON
[
    {
        "ratings": 5,
        "status": "inactive",
        "_id": "5ab2039f65739ca843d4bde5",
        "name": "test user",
        "phone": "123123123",
        "email": "testuser@gmail.com",
        "gender": "M",
        "age": 26,
        "drivingLicience": "LICIENCE123",
        "address": {
            "streetNumber": "43437",
            "street": "Fremont Boulevard",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94538",
            "full": "43437 Fremont Blvd, Fremont, CA 94538, USA",
            "lat": 37.515384,
            "lng": -121.9535577,
            "latLng": "37.515384,-121.9535577"
        },
        "createdAt": "2018-03-21T07:02:55.882Z",
        "updatedAt": "2018-03-21T07:02:55.932Z"
    },
    {
        "ratings": 5,
        "status": "active",
        "_id": "5ab60ef323890445bd922745",
        "name": "Rajan",
        "email": "rajankar471@gmail.com",
        "phone": "7895727782",
        "address": {
            "streetNumber": "287",
            "street": "McDuff Avenue",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94539",
            "full": "287 McDuff Ave, Fremont, CA 94539, USA",
            "lat": 37.4774154,
            "lng": -121.9198661,
            "latLng": "37.4774154,-121.9198661"
        },
        "drivingLicience": "DRLICIENCE1312",
        "age": 34,
        "gender": "M",
        "createdAt": "2018-03-24T08:40:19.587Z",
        "updatedAt": "2018-03-24T08:40:19.587Z"
    }
]
```

### GET based on status

/api/v1/drivers/?status=inactive

Response
```JSON
[
    {
        "ratings": 5,
        "status": "inactive",
        "_id": "5ab60ef323890445bd922745",
        "name": "Rajan",
        "email": "rajankar471@gmail.com",
        "phone": "7895727782",
        "address": {
            "streetNumber": "287",
            "street": "McDuff Avenue",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94539",
            "full": "287 McDuff Ave, Fremont, CA 94539, USA",
            "lat": 37.4774154,
            "lng": -121.9198661,
            "latLng": "37.4774154,-121.9198661"
        },
        "drivingLicience": "DRLICIENCE1312",
        "age": 34,
        "gender": "M",
        "createdAt": "2018-03-24T08:40:19.587Z",
        "updatedAt": "2018-03-24T08:40:19.587Z"
    }
]
```

### POST

POST request on this endpoint will create a new driver in the system.

POST object body:
```JSON
{
    "name": "Rajan",
    "email": "rajankar471@gmail.com",
    "phone":"7895727782",
    "address": {
        "streetAddress": "287 McDuff Avenue",
        "state": "CA",
        "city": "fremont",
        "zipCode": 94539
    },
    "drivingLicience": "DRLICIENCE1312",
    "age": 34,
    "gender": "M"
}
```

Endpoint Response:
```JSON
{
    "ratings": 5,
    "status": "inactive",
    "_id": "5ab60ef323890445bd922745",
    "name": "Rajan",
    "email": "rajankar471@gmail.com",
    "phone": "7895727782",
    "address": {
        "streetNumber": "287",
        "street": "McDuff Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94539",
        "full": "287 McDuff Ave, Fremont, CA 94539, USA",
        "lat": 37.4774154,
        "lng": -121.9198661,
        "latLng": "37.4774154,-121.9198661"
    },
    "drivingLicience": "DRLICIENCE1312",
    "age": 34,
    "gender": "M",
    "createdAt": "2018-03-24T08:40:19.587Z",
    "updatedAt": "2018-03-24T08:40:19.587Z"
}
```


## Endpoint: /api/v1/drivers/:driverid

This endpoint retrieve a certain driver info from the system. It can be used to update/retrieve a certain attribute as well by chnaging the Request Method type and query fields.

Supported REQUEST Methods:
- GET
- PUT
- DELETE

### GET

Example endpoints:
- /api/v1/drivers/5ab60ef323890445bd922745

Response:
```JSON
{
    "ratings": 5,
    "status": "inactive",
    "_id": "5ab60ef323890445bd922745",
    "name": "Rajan",
    "email": "rajankar471@gmail.com",
    "phone": "7895727782",
    "address": {
        "streetNumber": "287",
        "street": "McDuff Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94539",
        "full": "287 McDuff Ave, Fremont, CA 94539, USA",
        "lat": 37.4774154,
        "lng": -121.9198661,
        "latLng": "37.4774154,-121.9198661"
    },
    "drivingLicience": "DRLICIENCE1312",
    "age": 34,
    "gender": "M",
    "createdAt": "2018-03-24T08:40:19.587Z",
    "updatedAt": "2018-03-24T08:40:19.587Z"
}
```

### GET a certain attribute

Example endpoints:
- /api/v1/drivers/5ab60ef323890445bd922745?fields=name,address.zipCode,address.latLng

Response:
```JSON
{
    "_id": "5ab60ef323890445bd922745",
    "name": "Rajan",
    "address": {
        "zipCode": "94539",
        "latLng": "37.4774154,-121.9198661"
    }
}
```

### PUT

This Method can be used to update the driver attributes.

Example endpoint:
- /api/v1/drivers/5ab60ef323890445bd922745

For updating the status the request object body:
```JSON
{
  "status": "active"
}
```

Response:
```JSON
{
    "ratings": 5,
    "status": "active",
    "_id": "5ab60ef323890445bd922745",
    "name": "Rajan",
    "email": "rajankar471@gmail.com",
    "phone": "7895727782",
    "address": {
        "streetNumber": "287",
        "street": "McDuff Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94539",
        "full": "287 McDuff Ave, Fremont, CA 94539, USA",
        "lat": 37.4774154,
        "lng": -121.9198661,
        "latLng": "37.4774154,-121.9198661"
    },
    "drivingLicience": "DRLICIENCE1312",
    "age": 34,
    "gender": "M",
    "createdAt": "2018-03-24T08:40:19.587Z",
    "updatedAt": "2018-03-24T08:40:19.587Z"
}
```

### DELETE

This method simply updates the status of the Driver to inactive.

Example endpoint:
- /api/v1/drivers/5ab60ef323890445bd922745

Response:
```JSON
{
    "ratings": 5,
    "status": "inactive",
    "_id": "5ab60ef323890445bd922745",
    "name": "Rajan",
    "email": "rajankar471@gmail.com",
    "phone": "7895727782",
    "address": {
        "streetNumber": "287",
        "street": "McDuff Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94539",
        "full": "287 McDuff Ave, Fremont, CA 94539, USA",
        "lat": 37.4774154,
        "lng": -121.9198661,
        "latLng": "37.4774154,-121.9198661"
    },
    "drivingLicience": "DRLICIENCE1312",
    "age": 34,
    "gender": "M",
    "createdAt": "2018-03-24T08:40:19.587Z",
    "updatedAt": "2018-03-24T08:40:19.587Z"
}
```