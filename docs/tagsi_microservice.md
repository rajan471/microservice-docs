# Tagsi API

### Ride Formation JSON

```JSON
[
  {
    "rideType": "dropOff",
    "rideDetail": [
      {
        "driver": {
          "_id": "5aa66889164d2d06e2f0e4f2",
          "name": "Julia Brabo Santos",
          "phone": "242342342",
          "email": "123@fff.vom",
          "gender": "F",
          "age": 36,
          "drivingLicience": "DRLICIENCE4",
          "comments": []
        },
        "vehicle": {
          "_id": "5aa3581ad9059530ac259193",
          "openSeats": 3,
          "modelOfCar": "Mazda",
          "type": "CAR",
          "liciencePlateNumber": "LICIENCE3",
          "lastKnownLocation": {
            "lat": 37.57829650000001,
            "lng": -121.9943965
          }
        },
        "passengers": [
          {
            "_id": "5aa6708566043c9709356573",
            "name": "Student 2",
            "email": "student2@stu.dent",
            "phone": "82787235",
            "address": {
              "full": "3454 Bridgewood Terrace #212, Fremont, CA 94536, USA",
              "latLng": "37.5626729,-122.005485"
            },
            "notes": ""
          },
          {
            "_id": "5aa34f84d9059530ac259186",
            "name": "Student 5",
            "email": "student5@stud.dent",
            "phone": "123142343",
            "address": {
              "full": "4401 Central Ave, Fremont, CA 94536, USA",
              "latLng": "37.5512517,-122.0084994"
            },
            "notes": ""
          },
          {
            "_id": "5aa67a88a3acf38560358938",
            "name": "student7",
            "email": "student7@stud.ent",
            "phone": "72634762469",
            "address": {
              "full": "40734 Greystone Terrace, Fremont, CA 94538, USA",
              "latLng": "37.532972,-121.9676359"
            },
            "notes": ""
          }
        ],
        "partner": {
          "_id": "5ab6766da8d85845e1665214",
          "title": "California School for the Deaf",
          "email": "info@csdrcde.gov",
          "phone": "(+1)-951-248-7700",
          "address": {
              "full": "Middle School, 3044 Horace St, Riverside, CA 92506, USA",
              "latLng": "33.9413787,-117.3800547"
          }
        }
      }
    ]
  }
]
```