# API Gateway

- No login is considered at the moment.
- API gateway is just directing API calls to the respective APIs and acting as a proxy server.

### It allows the following Micro-Services to communicate
- Passenger
- Driver
- Place of Interest (POI)
- Partner
- Vehicle

### Services that are NOT exposed
- Notification

### It allows the following Mobility Micro-Services
- Tagsi


## URL format

The format URL stucture for different API request methods are as follows: 

#### GET

/api/v1/&lt;serviceid&gt;/&lt;microservice&gt;/&lt;id&gt;

Below are some examples:
- /api/v1/passengers/5ab0f28f462ffeef650019e
here service id is `null`.
- /api/v1/tagsi/availability/passengers/5ab0f28f462ffeef650019e
Here serviceid is `tagsi`.

#### POST

/api/v1/&lt;serviceid&gt;/&lt;microservice&gt;

Below are some examples:
- /api/v1/passengers

Here service id is `null`.
- /api/v1/tagsi/availability/passengers/

Here serviceid is `tagsi`.


#### PUT

/api/v1/&lt;serviceid&gt;/&lt;microservice&gt;/&lt;id&gt;

Below are some examples:
- /api/v1/passengers/5ab0f28f462ffeef650019e
here service id is `null`.
- /api/v1/tagsi/availability/passengers/5ab0f28f462ffeef650019e
Here serviceid is `tagsi`.

#### DELETE

/api/v1/&lt;serviceid&gt;/&lt;microservice&gt;/&lt;id&gt;

Below are some examples:
- /api/v1/passengers/5ab0f28f462ffeef650019e
here service id is `null`.
- /api/v1/tagsi/availability/passengers/5ab0f28f462ffeef650019e
Here serviceid is `tagsi`.