# Partner API

Domain: activedev.westus.cloudapp.azure.com

## Endpoint: /api/v1/places/

### GET

- GET all places (Should be called by Admin Console)

/api/v1/places/

Response
```JSON
[
    {
        "status": "active",
        "_id": "5ab67a1314535f460964873f",
        "name": "Coyote Hills Regional Park",
        "address": {
            "streetNumber": "8000",
            "street": "Patterson Ranch Road",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94555",
            "full": "8000 Patterson Ranch Rd, Fremont, CA 94555, USA",
            "lat": 37.5566285,
            "lng": -122.0940598,
            "latLng": "37.5566285,-122.0940598"
        },
        "createdAt": "2018-03-24T16:17:23.619Z",
        "updatedAt": "2018-03-24T16:17:23.619Z"
    }
]
```

### GET based on status

/api/v1/places/?status=inactive

Response
```JSON
[]
```

### POST

POST request on this endpoint will create a new place in the system.

POST object body:
```JSON
{
	"name": "Coyote Hills Regional Park",
	"address": {
		"streetAddress": "8000 Patterson Ranch Rd",
		"city": "Fremont",
		"state": "CA",
		"zipCode": 94555
	}
}
```

Endpoint Response:
```JSON
{
    "status": "active",
    "_id": "5ab67a1314535f460964873f",
    "name": "Coyote Hills Regional Park",
    "address": {
        "streetNumber": "8000",
        "street": "Patterson Ranch Road",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94555",
        "full": "8000 Patterson Ranch Rd, Fremont, CA 94555, USA",
        "lat": 37.5566285,
        "lng": -122.0940598,
        "latLng": "37.5566285,-122.0940598"
    },
    "createdAt": "2018-03-24T16:17:23.619Z",
    "updatedAt": "2018-03-24T16:17:23.619Z"
}
```


## Endpoint: /api/v1/places/:placeid

This endpoint retrieve a certain place info from the system. It can be used to update/retrieve a certain attribute as well by chnaging the Request Method type and query fields.

Supported REQUEST Methods:
- GET
- PUT
- DELETE

### GET

Example endpoints:
- /api/v1/places/5ab67a1314535f460964873f

Response:
```JSON
{
    "status": "active",
    "_id": "5ab67a1314535f460964873f",
    "name": "Coyote Hills Regional Park",
    "address": {
        "streetNumber": "8000",
        "street": "Patterson Ranch Road",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94555",
        "full": "8000 Patterson Ranch Rd, Fremont, CA 94555, USA",
        "lat": 37.5566285,
        "lng": -122.0940598,
        "latLng": "37.5566285,-122.0940598"
    },
    "createdAt": "2018-03-24T16:17:23.619Z",
    "updatedAt": "2018-03-24T16:17:23.619Z"
}
```

### GET a certain attribute

Example endpoints:
- /api/v1/places/5ab67a1314535f460964873f?fields=name,address.latLng

Response:
```JSON
{
    "_id": "5ab67a1314535f460964873f",
    "name": "Coyote Hills Regional Park",
    "address": {
        "latLng": "37.5566285,-122.0940598"
    }
}
```

### PUT

This Method can be used to update the place attributes.

Example endpoint:
- /api/v1/places/5ab67a1314535f460964873f

For updating the status the request object body:
```JSON
{
  "status": "inactive"
}
```

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab67a1314535f460964873f",
    "name": "Coyote Hills Regional Park",
    "address": {
        "streetNumber": "8000",
        "street": "Patterson Ranch Road",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94555",
        "full": "8000 Patterson Ranch Rd, Fremont, CA 94555, USA",
        "lat": 37.5566285,
        "lng": -122.0940598,
        "latLng": "37.5566285,-122.0940598"
    },
    "createdAt": "2018-03-24T16:17:23.619Z",
    "updatedAt": "2018-03-24T16:17:23.619Z"
}
```

### DELETE

This method simply updates the status of the Place to inactive.

Example endpoint:
- /api/v1/places/5ab67a1314535f460964873f

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab67a1314535f460964873f",
    "name": "Coyote Hills Regional Park",
    "address": {
        "streetNumber": "8000",
        "street": "Patterson Ranch Road",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94555",
        "full": "8000 Patterson Ranch Rd, Fremont, CA 94555, USA",
        "lat": 37.5566285,
        "lng": -122.0940598,
        "latLng": "37.5566285,-122.0940598"
    },
    "createdAt": "2018-03-24T16:17:23.619Z",
    "updatedAt": "2018-03-24T16:17:23.619Z"
}
```