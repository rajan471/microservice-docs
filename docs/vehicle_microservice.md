# Partner API

Domain: activedev.westus.cloudapp.azure.com

## Endpoint: /api/v1/vehicles/

### GET

- GET all vehicles (Should be called by Admin Console)

/api/v1/vehicles/

Response
```JSON
[
    {
        "status": "active",
        "_id": "5ab67ce9194186466792b9ab",
        "model": "Mazda3",
        "type": "CAR",
        "licencePlateNumber": "LIC029EV9DEF",
        "doors": 4,
        "seats": {
            "back": 5,
            "front": 2,
            "totalSeats": 7,
            "openSeats": 6,
            "wheelChair": 0
        },
        "owner": {
            "firstName": "Ann Elizabeth",
            "lastName": "Vandyk",
            "phone": "+1-829-323-0223",
            "email": "elizabeth@gmail.com",
            "address": "3362 Copper Leaf Dr. San Jose, CA 95132"
        },
        "modelYear": 2011,
        "trackingDeviceInfo": {
            "deviceName": "gt230",
            "vendor": "concox",
            "IMEI": "1234567890123456"
        },
        "createdAt": "2018-03-24T16:29:29.652Z",
        "updatedAt": "2018-03-24T16:29:29.652Z"
    }
]
```

### GET based on status

/api/v1/vehicles/?status=inactive

Response
```JSON
[]
```

### POST

POST request on this endpoint will create a new vehicle in the system.

POST object body:
```JSON
{
	"model": "Mazda3",
	"type": "CAR",
	"licencePlateNumber": "LIC029EV9DEF",
	"doors": 4,
	"seats": {
		"back":5,
		"front":2,
		"totalSeats":7,
		"openSeats":6,
		"wheelChair": 0
	},
	"owner": {
		"firstName": "Ann Elizabeth",
		"lastName": "Vandyk",
		"phone": "+1-829-323-0223",
		"email": "elizabeth@gmail.com",
		"address": "3362 Copper Leaf Dr. San Jose, CA 95132"
	},
	"modelYear": 2011,
	"trackingDeviceInfo": {
		"deviceName": "gt230",
		"vendor": "concox",
		"IMEI": "1234567890123456"
	}
}
```

Endpoint Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab67ce9194186466792b9ab",
    "model": "Mazda3",
    "type": "CAR",
    "licencePlateNumber": "LIC029EV9DEF",
    "doors": 4,
    "seats": {
        "back": 5,
        "front": 2,
        "totalSeats": 7,
        "openSeats": 6,
        "wheelChair": 0
    },
    "owner": {
        "firstName": "Ann Elizabeth",
        "lastName": "Vandyk",
        "phone": "+1-829-323-0223",
        "email": "elizabeth@gmail.com",
        "address": "3362 Copper Leaf Dr. San Jose, CA 95132"
    },
    "modelYear": 2011,
    "trackingDeviceInfo": {
        "deviceName": "gt230",
        "vendor": "concox",
        "IMEI": "1234567890123456"
    },
    "createdAt": "2018-03-24T16:29:29.652Z",
    "updatedAt": "2018-03-24T16:29:29.652Z"
}
```


## Endpoint: /api/v1/vehicles/:vehicleid

This endpoint retrieve a certain vehicle info from the system. It can be used to update/retrieve a certain attribute as well by chnaging the Request Method type and query fields.

Supported REQUEST Methods:
- GET
- PUT
- DELETE

### GET

Example endpoints:
- /api/v1/vehicles/5ab67ce9194186466792b9ab

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab67ce9194186466792b9ab",
    "model": "Mazda3",
    "type": "CAR",
    "licencePlateNumber": "LIC029EV9DEF",
    "doors": 4,
    "seats": {
        "back": 5,
        "front": 2,
        "totalSeats": 7,
        "openSeats": 6,
        "wheelChair": 0
    },
    "owner": {
        "firstName": "Ann Elizabeth",
        "lastName": "Vandyk",
        "phone": "+1-829-323-0223",
        "email": "elizabeth@gmail.com",
        "address": "3362 Copper Leaf Dr. San Jose, CA 95132"
    },
    "modelYear": 2011,
    "trackingDeviceInfo": {
        "deviceName": "gt230",
        "vendor": "concox",
        "IMEI": "1234567890123456"
    },
    "createdAt": "2018-03-24T16:29:29.652Z",
    "updatedAt": "2018-03-24T16:29:29.652Z"
}
```

### GET a certain attribute

Example endpoints:
- /api/v1/vehicles/5ab67ce9194186466792b9ab?fields=model,owner,seats.openSeats

Response:
```JSON
{
    "_id": "5ab67ce9194186466792b9ab",
    "model": "Mazda3",
    "seats": {
        "openSeats": 6
    },
    "owner": {
        "firstName": "Ann Elizabeth",
        "lastName": "Vandyk",
        "phone": "+1-829-323-0223",
        "email": "elizabeth@gmail.com",
        "address": "3362 Copper Leaf Dr. San Jose, CA 95132"
    }
}
```

### PUT

This Method can be used to update the vehicle attributes.

Example endpoint:
- /api/v1/vehicles/5ab67ce9194186466792b9ab

For updating the status the request object body:
```JSON
{
  "status": "active"
}
```

Response:
```JSON
{
    "status": "active",
    "_id": "5ab67ce9194186466792b9ab",
    "model": "Mazda3",
    "type": "CAR",
    "licencePlateNumber": "LIC029EV9DEF",
    "doors": 4,
    "seats": {
        "back": 5,
        "front": 2,
        "totalSeats": 7,
        "openSeats": 6,
        "wheelChair": 0
    },
    "owner": {
        "firstName": "Ann Elizabeth",
        "lastName": "Vandyk",
        "phone": "+1-829-323-0223",
        "email": "elizabeth@gmail.com",
        "address": "3362 Copper Leaf Dr. San Jose, CA 95132"
    },
    "modelYear": 2011,
    "trackingDeviceInfo": {
        "deviceName": "gt230",
        "vendor": "concox",
        "IMEI": "1234567890123456"
    },
    "createdAt": "2018-03-24T16:29:29.652Z",
    "updatedAt": "2018-03-24T16:29:29.652Z"
}
```

### DELETE

This method simply updates the status of the Vehicle to inactive.

Example endpoint:
- /api/v1/vehicles/5ab67ce9194186466792b9ab

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab67ce9194186466792b9ab",
    "model": "Mazda3",
    "type": "CAR",
    "licencePlateNumber": "LIC029EV9DEF",
    "doors": 4,
    "seats": {
        "back": 5,
        "front": 2,
        "totalSeats": 7,
        "openSeats": 6,
        "wheelChair": 0
    },
    "owner": {
        "firstName": "Ann Elizabeth",
        "lastName": "Vandyk",
        "phone": "+1-829-323-0223",
        "email": "elizabeth@gmail.com",
        "address": "3362 Copper Leaf Dr. San Jose, CA 95132"
    },
    "modelYear": 2011,
    "trackingDeviceInfo": {
        "deviceName": "gt230",
        "vendor": "concox",
        "IMEI": "1234567890123456"
    },
    "createdAt": "2018-03-24T16:29:29.652Z",
    "updatedAt": "2018-03-24T16:29:29.652Z"
}
```