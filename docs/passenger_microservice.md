# Passsenger API

Domain: activedev.westus.cloudapp.azure.com

## Endpoint: /api/v1/passengers/

### GET

- GET all passengers (Should be called by Admin Console)

/api/v1/passengers/

Response
```JSON
[
    {
        "status": "active",
        "_id": "5ab5c42b742e55459dc5ba10",
        "name": "Rajan",
        "phone": "7895727782",
        "email": "rajank@activescaler.in",
        "age": 26,
        "gender": "M",
        "address": {
            "streetNumber": "4555",
            "street": "Thornton Avenue",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94536",
            "full": "4555 Thornton Ave #34, Fremont, CA 94536, USA",
            "lat": 37.554324,
            "lng": -122.0170369,
            "latLng": "37.554324,-122.0170369"
        },
        "createdAt": "2018-03-24T03:21:15.770Z",
        "updatedAt": "2018-03-24T03:21:15.770Z"
    },
    {
        "status": "active",
        "_id": "5ab5c4da742e55459dc5ba12",
        "name": "dinesh",
        "phone": "9818490692",
        "email": "dinesh@activescaler.in",
        "age": 27,
        "gender": "M",
        "address": {
            "streetNumber": "3615",
            "street": "Main Street",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94538",
            "full": "3615 Main St, Fremont, CA 94538, USA",
            "lat": 37.5339902,
            "lng": -121.9554508,
            "latLng": "37.5339902,-121.9554508"
        },
        "createdAt": "2018-03-24T03:24:10.500Z",
        "updatedAt": "2018-03-24T03:24:10.500Z"
    }
]
```

### GET based on status

/api/v1/passengers/?status=inactive

Response
```JSON
[
    {
        "status": "inactive",
        "_id": "5ab5c4da742e55459dc5ba12",
        "name": "dinesh",
        "phone": "9818490692",
        "email": "dinesh@activescaler.in",
        "age": 27,
        "gender": "M",
        "address": {
            "streetNumber": "3615",
            "street": "Main Street",
            "city": "Fremont",
            "county": "Alameda County",
            "state": "California",
            "zipCode": "94538",
            "full": "3615 Main St, Fremont, CA 94538, USA",
            "lat": 37.5339902,
            "lng": -121.9554508,
            "latLng": "37.5339902,-121.9554508"
        },
        "createdAt": "2018-03-24T03:24:10.500Z",
        "updatedAt": "2018-03-24T03:24:10.500Z"
    }
]
```

### POST

POST request on this endpoint will create a new passenger in the system.

POST object body:
```JSON
{
	"name": "Rajan",
	"phone": "7895727782",
	"email": "rajank@activescaler.in",
	"age": 26,
	"gender": "M",
	"address": {
		"streetAddress": "4555 Thornton Ave Apt #34",
		"state": "CA",
		"city": "Fremont",
		"zipCode": 94536
	}
}
```

Endpoint Response:
```JSON
{
    "status": "active",
    "_id": "5ab5c42b742e55459dc5ba10",
    "name": "Rajan",
    "phone": "7895727782",
    "email": "rajank@activescaler.in",
    "age": 26,
    "gender": "M",
    "address": {
        "streetNumber": "4555",
        "street": "Thornton Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94536",
        "full": "4555 Thornton Ave #34, Fremont, CA 94536, USA",
        "lat": 37.554324,
        "lng": -122.0170369,
        "latLng": "37.554324,-122.0170369"
    },
    "createdAt": "2018-03-24T03:21:15.770Z",
    "updatedAt": "2018-03-24T03:21:15.770Z"
}
```


## Endpoint: /api/v1/passengers/:passengerid

This endpoint retrieve a certain passenger info from the system. It can be used to update/retrieve a certain attribute as well by chnaging the Request Method type and query fields.

Supported REQUEST Methods:
- GET
- PUT
- DELETE

### GET

Example endpoints:
- /api/v1/passengers/5ab5c42b742e55459dc5ba10

Response:
```JSON
{
    "status": "active",
    "_id": "5ab5c42b742e55459dc5ba10",
    "name": "Rajan",
    "phone": "7895727782",
    "email": "rajank@activescaler.in",
    "age": 26,
    "gender": "M",
    "address": {
        "streetNumber": "4555",
        "street": "Thornton Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94536",
        "full": "4555 Thornton Ave #34, Fremont, CA 94536, USA",
        "lat": 37.554324,
        "lng": -122.0170369,
        "latLng": "37.554324,-122.0170369"
    },
    "createdAt": "2018-03-24T03:21:15.770Z",
    "updatedAt": "2018-03-24T03:21:15.770Z"
}
```

### GET a certain attribute

Example endpoints:
- /api/v1/passengers/5ab5c42b742e55459dc5ba10?fields=name,address.zipCode,address.latLng

Response:
```JSON
{
    "_id": "5ab5c42b742e55459dc5ba10",
    "name": "Rajan",
    "address": {
        "zipCode": "94536",
        "latLng": "37.554324,-122.0170369"
    }
}
```

### PUT

This Method can be used to update the passenger attributes.

Example endpoint:
- /api/v1/passengers/5ab5c42b742e55459dc5ba10

For updating the status the request object body:
```JSON
{
  "status": "inactive"
}
```

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab5c42b742e55459dc5ba10",
    "name": "Rajan",
    "phone": "7895727782",
    "email": "rajank@activescaler.in",
    "age": 26,
    "gender": "M",
    "address": {
        "streetNumber": "4555",
        "street": "Thornton Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94536",
        "full": "4555 Thornton Ave #34, Fremont, CA 94536, USA",
        "lat": 37.554324,
        "lng": -122.0170369,
        "latLng": "37.554324,-122.0170369"
    },
    "createdAt": "2018-03-24T03:21:15.770Z",
    "updatedAt": "2018-03-24T03:21:15.770Z"
}
```

### DELETE

This method simply updates the status of the Passenger to inactive.

Example endpoint:
- /api/v1/passengers/5ab5c42b742e55459dc5ba10

Response:
```JSON
{
    "status": "inactive",
    "_id": "5ab5c42b742e55459dc5ba10",
    "name": "Rajan",
    "phone": "7895727782",
    "email": "rajank@activescaler.in",
    "age": 26,
    "gender": "M",
    "address": {
        "streetNumber": "4555",
        "street": "Thornton Avenue",
        "city": "Fremont",
        "county": "Alameda County",
        "state": "California",
        "zipCode": "94536",
        "full": "4555 Thornton Ave #34, Fremont, CA 94536, USA",
        "lat": 37.554324,
        "lng": -122.0170369,
        "latLng": "37.554324,-122.0170369"
    },
    "createdAt": "2018-03-24T03:21:15.770Z",
    "updatedAt": "2018-03-24T03:21:15.770Z"
}
```